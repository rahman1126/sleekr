<?php

function someTest()
{
	if (!isSuccess(doProcess1())) {
		return _PROCESS1FAILED;
	}

	if (!isSuccess(doProcess2())) {
		return _PROCESS2FAILED;
	}

	if (!isSuccess(doProcess3())) {
		return _PROCESS3FAILED;
	}

	if (!isSuccess(doProcess4())) {
		return _PROCESS4FAILED;
	}

	return _OK;
}

?>